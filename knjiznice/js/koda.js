
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
window.addEventListener('load' , function(){
    document.querySelector("#generiranje").addEventListener('click', function() {
        generirajPodatke(1);
        generirajPodatke(2);
        generirajPodatke(3);
    });
});

var niSeIzpisano = 1;
var cnt = 1;
var ehrLebron = "9bc6a0ee-a3b1-4073-aecc-187d3c72cba8";
var ehrJansa = "24d1a92b-75f0-4123-9a52-e63a78891448";
var ehrRibe = "6ba6eb85-99c1-4efc-b6bf-16a4256f49da";

function generirajPodatke(stPacienta) {
    var ehrId = "";
    var sessionId = getSessionId();
    
    if(stPacienta == 1 && niSeIzpisano) {
        $.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        ehrId = data.ehrId;
		        var partyData = {
		            firstNames: "Janez",
		            lastNames: "Janša",
		            dateOfBirth: "1958-09-17T00:00:00.000Z",
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
				// Dodamo še vitalne znake
				var telesnaTemperatura = "34";
				var sistolicniKrvniTlak = "100";
				var diastolicniKrvniTlak = "100";
				var nasicenostKrviSKisikom = "98";
		        var extraPartyData = {
				    "ctx/language": "en",
				    "ctx/territory": "SI",
				   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
				    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
				    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
				    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
				    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
				};
				var queryParams = {
				    ehrId: ehrId,
				    templateId: 'Vital Signs',
				    format: 'FLAT',
				    committer: 'Doctor Who'
				};
		        $.ajax({
		            url: baseUrl + "/demographics/party",
	                type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                	var barva = (cnt % 2 == 0) ? "success" : "info";
		                    $("#dodajanjeGeneriranih").append(`<div class="panel-body bg-${barva}"><p>`+
		                    `EhrId ${cnt}. vzorčnega pacienta: <b>${ehrId}</b><p></div>`);
		                    cnt++;
							ehrJansa = ehrId;
		                }
		            },
		            error: function(err) {
		            	console.log("Napaka: "+ err);
		            }
		        });
		    }
		});
		return ehrId;
    } else if(stPacienta == 2 && niSeIzpisano) {
        $.ajaxSetup({
		    headers: {
		    	"Ehr-Session": sessionId
		    }
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: "LeBron",
		            lastNames: "James",
		            dateOfBirth: "1984-12-30T00:00:00.000Z",
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        // Dodamo še vitalne znake
				var sessionId = getSessionId();
				var telesnaTemperatura = "37";
				var sistolicniKrvniTlak = "80";
				var diastolicniKrvniTlak = "120";
				var nasicenostKrviSKisikom = "100";
		        var extraPartyData = {
				    "ctx/language": "en",
				    "ctx/territory": "SI",
				   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
				    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
				    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
				    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
				    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
				};
				var queryParams = {
				    ehrId: ehrId,
				    templateId: 'Vital Signs',
				    format: 'FLAT',
				    committer: 'Doctor Who'
				};
		        $.ajax({
		            url: baseUrl + "/demographics/party",
	                type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                	var barva = (cnt % 2 == 0) ? "success" : "info";
		                    $("#dodajanjeGeneriranih").append(`<div class="panel-body bg-${barva}"><p>`+
		                    `EhrId ${cnt}. vzorčnega pacienta: <b>${ehrId}</b><p></div>`);
		                    cnt++;
							ehrLebron = ehrId;
		                }
		            },
		            error: function(err) {
		            	console.log("Napaka: "+ err);
		            }
		        });
		    }
		});
		return ehrId;
    } else if(stPacienta == 3 && niSeIzpisano) {
        $.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: "Janko",
		            lastNames: "Ribežnik",
		            dateOfBirth: "1998-03-18T00:00:00.000Z",
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        // Dodamo še vitalne znake
				var sessionId = getSessionId();
				var telesnaTemperatura = "41";
				var sistolicniKrvniTlak = "65";
				var diastolicniKrvniTlak = "100";
				var nasicenostKrviSKisikom = "95";
		        var extraPartyData = {
				    "ctx/language": "en",
				    "ctx/territory": "SI",
				   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
				    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
				    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
				    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
				    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
				};
				var queryParams = {
				    ehrId: ehrId,
				    templateId: 'Vital Signs',
				    format: 'FLAT',
				    committer: 'Doctor Who'
				};
		        $.ajax({
		            url: baseUrl + "/demographics/party",
	                type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                	var barva = (cnt % 2 == 0) ? "success" : "info";
		                    $("#dodajanjeGeneriranih").append(`<div class="panel-body bg-${barva}"><p>`+
		                    `EhrId ${cnt}. vzorčnega pacienta: <b>${ehrId}</b><p></div>`);
							ehrRibe = ehrId;
		                    cnt++;
		                }
		            },
		            error: function(err) {
		            	console.log("Napaka: "+ err);
		            }
		        });
		    }
		});
        niSeIzpisano = 0;
		return ehrId;
    }
    return ehrId;
}

// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

var stevec = 0;

function kreirajEHRzaBolnika() {
	var sessionId = getSessionId();
	var ime = $("#ime").val();
	var priimek = $("#priimek").val();
	var datumRojstva = $("#datumRojstva").val() + "T00:00:00.000Z";

	$.ajaxSetup({
    	headers: {
        	"Ehr-Session": sessionId
    	}
	});
	$.ajax({
    	url: baseUrl + "/ehr",
    	type: 'POST',
    	success: function (data) {
	    	var ehrId = data.ehrId;

	        // build party data
	        var partyData = {
	            firstNames: ime,
	            lastNames: priimek,
	            dateOfBirth: datumRojstva,
	            partyAdditionalInfo: [{key: "ehrId",value: ehrId}]
	        };
	        $.ajax({
	            url: baseUrl + "/demographics/party",
	            type: 'POST',
	            contentType: 'application/json',
	            data: JSON.stringify(partyData),
	            success: function (party) {
	                if (party.action == 'CREATE') {
						stevec++;
				    	var barva = (stevec % 2 == 0) ?  "success" : "info" ;
				        $("#dodajEHR").append(`<div class="panel-body bg-${barva}"><p>`+
						`EhrId novega uporabnika: <b>${ehrId}</b><p></div>`);
	                }
	            }
	        });
    	}
	});
}

function vnesiVitalnePodatke() {
	var sessionId = getSessionId();
    var ehrId = $("#ehrId").val();
	var telesnaTemperatura = $("#telesnaTemperatura").val();
	var sistolicniKrvniTlak = $("#sistolicniTlak").val();
	var diastolicniKrvniTlak = $("#diastolicniTlak").val();
	var nasicenostKrviSKisikom = $("#nasicenostKisika").val();

	
	$.ajaxSetup({
	    headers: {"Ehr-Session": sessionId}
	});
	var extraPartyData = {
	    "ctx/language": "en",
	    "ctx/territory": "SI",
	   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
	    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
	    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
	    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
	    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
	};
	var queryParams = {
	    ehrId: ehrId,
	    templateId: 'Vital Signs',
	    format: 'FLAT',
	    committer: 'Doctor Who'
	};
	$.ajax({
	    url: baseUrl + "/composition?" + $.param(queryParams),
	    type: 'POST',
	    contentType: 'application/json',
	    data: JSON.stringify(extraPartyData),
	    success: function (res) {
	        console.log("success");
	    },
	    error: function(err) {
	    	console.log("Napaka: "+err);
	    }
	});
}

function vrniOcenoZaEhr() {
	var ehrId = "";
	var sessionId = getSessionId();
	
	$.ajax({
		url: baseUrl + "/view/"+ehrId+"/body_temperature",
		type: 'GET',
		headers: {"Ehr-Session": sessionId},
    	success: function (data) {
			var party = data.party;
			console.log(party);
			
		},
		error: function(err) {
			console.log("Napaka: "+err);
		}
	});
}
$(document).ready(function() {
	$('#izbranPacient').change(function() {
		var id= "";
		var izbranPacient = $("#izbranPacient").val();
		if (izbranPacient == "LeBron James") id = ehrLebron;
		else if(izbranPacient == "Janez Janša")id = ehrJansa;
		else if(izbranPacient =="Janko Ribežnik") id = ehrRibe;
		$("#preberiEhrId").val(id);
	});
});